Welcome to the home of the Atlassian Marshaller Protobuf.

## Issues
Raise issues in [MARSHAL](https://ecosystem.atlassian.net/projects/MARSHAL/summary).

## CI Builds
The CI builds are setup on [ecosystem-bamboo](https://ecosystem-bamboo.internal.atlassian.com/browse/MARSHAL-PROTOBUFMARSHALM). They are done using a [plan template build](https://ecosystem-bamboo.internal.atlassian.com/browse/MARSHAL-PLAN).

## Versioning
This library is using the [Semantic Versioning 2.0.0](http://semver.org/spec/v2.0.0.html) approach for version numbers.

## Releases
Use the CI build `Release` stage.

## Building
The prerequisites for building are:

- Java 8 - if you're using [jEnv](http://www.jenv.be/) then the required `.java-version` file exists.
- Maven 3.3.9 - if you're using [Maven Version Manager](http://mvnvm.org/) then the required `mvmvm.properties` file exists.

To build, use the following:

    mvn clean verify

Recommend using `mvn javadoc:javadoc` to get the Javadoc links verified.

## Code Conventions
Follow the [Platform Rules Of Engagement](http://go.atlassian.com/proe) for committing to this project.
