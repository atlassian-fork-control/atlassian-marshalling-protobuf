/**
 * Contains support for marshalling Google Protocol Buffers (protobuf).
 *
 * @since 1.0.0
 */
@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
@FieldsAreNonnullByDefault
package com.atlassian.marshalling.protobuf;

import com.atlassian.annotations.nonnull.FieldsAreNonnullByDefault;
import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
